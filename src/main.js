import { createApp } from 'vue'
import { Quasar } from 'quasar'

import App from './App.vue'
import router from './router'
import mitt from 'mitt'
import quasarUserOptions from './quasar-user-options'

const app = createApp(App)

app.config.globalProperties.emitter = mitt()
app.use(Quasar, quasarUserOptions).use(router).mount('#app')
